#!/usr/bin/env python3

import subprocess
import sys
from collections import namedtuple
from os import environ, path
from typing import List, Tuple

from mypy import api

class CheckError(Exception):
    def __init__(self, message):
        self.message = message

BColors = namedtuple('BColors', ['HEADER', 'OK_BLUE', 'OK_GREEN', 'WARNING',
                                 'FAIL', 'ENDC', 'BOLD', 'UNDERLINE'])
BCOLORS = BColors(HEADER='\033[95m',
                  OK_BLUE='\033[94m',
                  OK_GREEN='\033[92m',
                  WARNING='\033[93m',
                  FAIL='\033[91m',
                  ENDC='\033[0m',
                  BOLD='\033[1m',
                  UNDERLINE='\033[4m')

PREFIX: str = environ.get('MESON_SOURCE_ROOT')
SRCFILES: List[str] = [
  'main.py',
  'application.py',
  'window.py',
]

def run_type_analizer() -> None:
    print(BCOLORS.HEADER + '############################################'+ BCOLORS.ENDC)
    print(BCOLORS.HEADER + '    Running type analysis with Mypy...      ' + BCOLORS.ENDC)
    print(BCOLORS.HEADER + '############################################'+ BCOLORS.ENDC)
    types_are_good: bool = True
    for srcfile in SRCFILES:
        result: Tuple[str, str, int] = api.run([path.join(PREFIX, 'baseapp', srcfile),
                                                '--follow-imports=skip',
                                                '--ignore-missing-imports',
                                                # Gtk.FlowBox and such
                                                '--allow-subclassing-any',
                                                # Gtk.Callback and such
                                                '--allow-untyped-decorators',
                                                '--strict'])
        if result[0] != "":
            print(BCOLORS.FAIL + 'Type checking has the following errors' + BCOLORS.ENDC)
            print(result[0])
            types_are_good = False
        if result[1] != "":
            print(BCOLORS.FAIL + 'Type checking has failed' + BCOLORS.ENDC)
            print(result[1])
            types_are_good = False

    if types_are_good:
        print(f'{BCOLORS.OK_GREEN}############################################{BCOLORS.ENDC}')
        print(f'{BCOLORS.OK_GREEN}           🌱 Types are all good 🌱        {BCOLORS.ENDC}')
        print(f'{BCOLORS.OK_GREEN}############################################{BCOLORS.ENDC}')
    else:
        raise CheckError('Type checking failed')

print('\n')
if __name__ == "__main__":
    import os
    # Change current working path to the this file parent's, then change to the
    # root folder. This is done so imports checking work properly, as they would
    # be imported once installed
    os.chdir(os.path.dirname(__file__))
    os.chdir('../../')

    run_type_analizer()
