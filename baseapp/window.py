# window.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from dataclasses import dataclass
from typing import Dict, Any, List

from gi.repository import Gtk, Gio, GLib

@Gtk.Template.from_resource('/org/gnome/BaseApp/window.ui')
class Window(Gtk.ApplicationWindow):
    __gtype_name__ = 'Window'

    main_grid: Gtk.Paned = Gtk.Template.Child('main_grid')

    def __init__(self, **kwargs: Dict[str, Any]) -> None:
        super().__init__(**kwargs)
