# main.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import sys
import traceback
from typing import Any, Dict
import logging

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk
from baseapp.application import Application

def on_unhandled_exception(_exception_type: BaseException,
                           _value: BaseException, _trace_back: Any) -> None:
    traceback.print_last()
    sys.exit(-1)

def options() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Python 3.7 with typing enforcement base app")

    return parser.parse_args()

def main(_version: int, **_kwargs: Any) -> int:
    # Exit the application on unhandled exception.
    # PyGobject was just printing it for some reason, makes things hard to debug.
    sys.excepthook = on_unhandled_exception # type: ignore
    cli_args = options()
    app: Gtk.Application = Application()

    return int(app.run(sys.argv))
