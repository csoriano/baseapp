# application.py
#
# Copyright 2018 Carlos Soriano <csoriano@redhat.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later

import threading

import gi
from gi.repository import Gtk, Gio, Gdk
from baseapp.window import Window

class Application(Gtk.Application):
    __gtype_name__ = 'Application'

    def __init__(self) -> None:
        super().__init__(application_id='org.gnome.BaseApp',
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self) -> None:
        win: Gtk.Window = self.get_active_window()
        if not win:
            win = Window(application=self)
        self._init_style(win)
        win.present()

    @staticmethod
    def _init_style(win: Window) -> None:
        css_provider_file = Gio.File.new_for_uri('resource:///org/gnome/BaseApp/application.css')
        css_provider = Gtk.CssProvider()
        css_provider.load_from_file(css_provider_file)
        display = Gdk.Display.get_default()
        style_context = win.get_style_context()
        style_context.add_provider_for_display(display, css_provider,
                                               Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

